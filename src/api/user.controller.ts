import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { PeopleRegisterDTO } from '../dto/persona.dto';
import { UserService } from './user.service';

@Controller('people')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  register(@Body() people: PeopleRegisterDTO): number {
    return this.userService.register(people);
  }
}
