import { Injectable } from '@nestjs/common';
import { PeopleRegisterDTO } from 'src/dto/persona.dto';

@Injectable()
export class UserService {
  /**
   * register an persona
   * @param PersonaDTO
   * @return object
   */

  register(persona: PeopleRegisterDTO): number {
    const { nombre, edad, sexo, altura, peso } = persona;

    const isIMC = this._calcularIMC(peso, altura, sexo);
    const isValidAge = this._isOlder(edad);
    const isValidSex = this._comprobarSexo(sexo);
    const generateNss = this._generateNSS(8);
    console.log(generateNss);

    return isIMC;
  }

  _isOlder(age: number): boolean {
    return age > 17 ? true : false;
  }

  _comprobarSexo(sex: string): boolean {
    return sex === 'F' || sex === 'M' ? true : false;
  }

  _generateNSS(num: number): string {
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result1 = ' ';
    const charactersLength = characters.length;
    for (let i = 0; i < num; i++) {
      result1 += characters.charAt(
        Math.floor(Math.random() * charactersLength),
      );
    }

    return result1;
  }
  _calcularIMC(peso: number, altura: number, sexo: string): number {
    const imcx = peso / (altura * altura);

    switch (sexo) {
      case 'F':
        if (imcx <= 19) {
          return -1;
        }
        if (imcx >= 19 || imcx <= 24) {
          return 0;
        }
        if (imcx > 24) {
          return 1;
        }
        break;

      case 'M':
        if (imcx <= 19) {
          return -1;
        }
        if (imcx >= 20 || imcx <= 25) {
          return 0;
        }
        if (imcx > 20) {
          return 1;
        }
        break;

      default:
        return null;
    }
  }
}
