import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Client, Information } from '../interfaces/client.interface';
import { ClientDTO, InformationDTO } from '../../../dto/client.dto';

@Injectable()
export class ClientModel {
  private logger: Logger = new Logger('ClientModel');

  constructor(@InjectModel('client') readonly clientModel: Model<Client>) {}

  /**
   * save the client register in the data base
   * @param clientDTO
   * @return client
   */

  async create(clientDTO: ClientDTO | any): Promise<Client> {
    const client = this.clientModel.create(clientDTO);
    return await client;
  }

  /**
   * Find client by id
   * @param clientId The client's id
   * @return Found client
   */
  async findById(clientId?: string): Promise<Client | Client[]> {
    let client;
    if (clientId) {
      client = await this.clientModel.findOne({ _id: clientId });
    }

    client = await this.clientModel.find({});

    return client;
  }

  /**
   * Update clients
   * @param _id The clients id
   * @param client The clients
   * @return client
   */
  async updateInformation(_id: string, information: InformationDTO) {
    const result = await this.clientModel.findOneAndUpdate(
      { _id },
      information,
      { new: true },
    );

    return result;
  }
}
