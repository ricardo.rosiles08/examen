import { EnvironmentVariablesModule } from '../../../config/environments/environmentVariables.module';
import { ClientModel } from './client.model';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { ClientDTO } from 'src/dto/client.dto';

const mockClientModel = {
  create: jest.fn(),
};

describe('Client Model', () => {
  let clientModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [EnvironmentVariablesModule],
      providers: [
        ClientModel,
        {
          provide: getModelToken('client'),
          useValue: mockClientModel,
        },
      ],
    }).compile();

    clientModel = module.get<ClientModel>(ClientModel);
  });

  it('Should be defined', () => {
    expect(clientModel).toBeDefined();
  });

  it('Create Client', async () => {
    const document: ClientDTO = {
      name: 'test',
      lastName: 'testing',
      email: 'testing@gmail.com',
      password: '1234567890',
      userName: 'testing12345',
    };
    mockClientModel.create.mockResolvedValue(document);

    const result = await clientModel.create(document);

    expect(result).toEqual(document);
  });
});
