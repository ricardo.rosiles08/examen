import { Schema, HookNextFunction, Types } from 'mongoose';
import { hash } from 'bcrypt';
import * as _ from 'lodash';
import { REGEX_EMAIL, SALT_ROUNDS_HASH_PASSWORD } from 'src/consts';

export const ClientSchema = new Schema({
  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    validate: [REGEX_EMAIL, 'badEmail'],
  },
  name: {
    type: String,
    required: 'requiredField name',
  },
  lastName: {
    type: String,
    required: 'requiredField lastName',
  },
  password: { type: String },
  userName: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
  age: { type: Number },
  height: { type: Types.Decimal128 },
  weight: { type: Types.Decimal128 },
  imc: { type: Types.Decimal128 },
  geb: { type: Types.Decimal128 },
  eta: { type: Types.Decimal128 },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: null },
});

ClientSchema.index({ email: 1 }, { unique: true });
ClientSchema.index({ userName: 1 }, { unique: true });

ClientSchema.pre('save', async function (next: HookNextFunction) {
  const user = this;

  if (user['password'] && user.isModified('password')) {
    user['password'] = await hash(user['password'], SALT_ROUNDS_HASH_PASSWORD);
  }

  return next();
});
