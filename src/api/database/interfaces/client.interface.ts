export interface Client extends Document {
  _id: string;
  email: string;
  name: string;
  lastName: string;
  password: string;
}

export interface Information extends Document {
  _id: string;
  email: string;
  name: string;
  lastName: string;
  password: string;
  age: number;
  height: number;
  weight: number;
  geb: number;
}
