import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ClientDTO, InformationDTO } from '../../dto/client.dto';
import { Client } from '../database/interfaces/client.interface';
import { ClientService } from './client.service';

@Controller('nutriNet')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Post('client')
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() client: ClientDTO): Promise<Client> {
    const data = await this.clientService.register(client);
    return data;
  }

  @Get('client')
  @HttpCode(HttpStatus.CREATED)
  async getClients(@Body() clientId?: string): Promise<Client | Client[]> {
    const data = await this.clientService.getClients(clientId);
    return data;
  }
  @Put('client')
  @HttpCode(HttpStatus.CREATED)
  async updateClients(
    @Param('clientId') clientId: string,
    @Body() info: InformationDTO,
  ) {
    const data = await this.clientService.updateClients(clientId, info);
    return data;
  }
}
