import { Test, TestingModule } from '@nestjs/testing';
import { ClientDTO } from '../../dto/client.dto';
import { ClientModel } from '../database/models/client.model';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';

describe('AppController', () => {
  const mockClientModel = () => ({
    create: jest.fn(),
  });
  let clientService: ClientService;
  let clientModel;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientController],
      providers: [
        {
          provide: ClientModel,
          useFactory: mockClientModel,
        },
        ClientService,
      ],
    }).compile();

    clientService = module.get<ClientService>(ClientService);
    clientModel = module.get<ClientModel>(ClientModel);
  });

  it('Should be defined', () => {
    expect(clientService).toBeDefined();
    expect(clientModel).toBeDefined();
  });

  it('Register Admin with Password', async () => {
    const payload = {
      _id: 'abc1234',
      name: 'test',
      lastName: 'testing',
      email: 'testing123@gmail.com',
      password: 'Password1.',
      userName: 'testing123',
    };

    const resolveClient = {
      _id: payload._id,
      name: payload.name,
      lastName: payload.lastName,
      email: payload.email,
      userName: payload.userName,
      password: payload.password,
    };

    clientModel.create.mockResolvedValue(resolveClient);

    const register: ClientDTO = {
      name: 'test',
      lastName: 'testing',
      email: 'testing123@gmail.com',
      password: 'Password1.',
      userName: 'testing123',
    };

    const result = await clientService.register(register);

    const resultExpect = {
      _id: payload._id,
      name: payload.name,
      lastName: payload.lastName,
      email: payload.email,
      userName: payload.userName,
      password: payload.password,
    };

    expect(result).toEqual(resultExpect);
  });
});
