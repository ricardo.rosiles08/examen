import { Injectable } from '@nestjs/common';
import { ClientDTO, InformationDTO } from '../../dto/client.dto';
import { ClientModel } from '../database/models/client.model';
import * as _ from 'lodash';
import { Client } from '../database/interfaces/client.interface';

@Injectable()
export class ClientService {
  constructor(private readonly clientModel: ClientModel) {}
  /**
   * register an persona
   * @param ClientDTO
   * @return object
   */
  async register(clientDTO: ClientDTO): Promise<Client> {
    const creationData: any = _.pick(clientDTO, [
      'email',
      'name',
      'lastName',
      'password',
      'userName',
    ]);

    const client = await this.clientModel.create(creationData);

    return client;
  }
  async getClients(clientId?: string): Promise<Client | Client[]> {
    return this.clientModel.findById(clientId);
  }
  async updateClients(clientId: string, info: InformationDTO) {
    return this.clientModel.updateInformation(clientId, info);
  }
}
