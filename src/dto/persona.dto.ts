import { IsNumber, IsOptional, ValidateIf } from 'class-validator';
import { IsNotEmptyString } from '../decorators';

/**
 * Prefix readonly is used to make a property as read-only.
 */
export class PeopleRegisterDTO {
  @IsNotEmptyString()
  readonly nombre?: string;

  @IsNumber()
  readonly edad: number;

  @IsOptional()
  readonly nss?: string;

  @ValidateIf((sex) => sex === 'M' || sex === 'F')
  @IsNotEmptyString()
  readonly sexo: string;

  @IsNumber({ maxDecimalPlaces: 2 })
  readonly peso: number;

  @IsNumber({ maxDecimalPlaces: 2 })
  readonly altura: number;
}
