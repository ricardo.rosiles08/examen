import {
  IsDecimal,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength,
  ValidateIf,
} from 'class-validator';
import { MAX_PASSWORD, MIN_PASSWORD, REGEX_PASSWORD } from '../consts';
import { IsNotEmptyString } from '../decorators';
export class ClientDTO {
  @IsNotEmptyString()
  readonly name?: string;

  @IsNumber()
  readonly lastName: string;

  @IsNotEmptyString()
  readonly userName: string;

  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @IsOptional()
  @ValidateIf((o) => o.password)
  @IsString()
  @MinLength(MIN_PASSWORD)
  @MaxLength(MAX_PASSWORD)
  @Matches(REGEX_PASSWORD)
  readonly password?: string;
}

export class InformationDTO {
  @IsDecimal()
  age: number;

  @IsDecimal()
  height: number;

  @IsDecimal()
  weight: number;

  @IsDecimal()
  geb: number;
}
