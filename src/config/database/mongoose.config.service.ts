import { EnvironmentVariablesService } from '../environments/environmentVariables.service';
import { Injectable, Logger } from '@nestjs/common';
import { MongooseOptionsFactory } from '@nestjs/mongoose';

@Injectable()
export class MongooseConfigService implements MongooseOptionsFactory {
  private logger: Logger = new Logger('MongooseConfigService');
  private uri: string;

  constructor(config: EnvironmentVariablesService) {
    if (config.getNodeEnv() !== 'production') {
      this.logger.log(`Host: ${config.getDatabaseHost()}`);
      this.logger.log(`Port: ${config.getDatabasePort()}`);
      this.logger.log(`Name: ${config.getDatabaseName()}`);
    } else {
      this.uri = `mongodb+srv://${config.getDatabaseUsername()}:${config.getDatabasePassword()}@${config.getDatabaseHost()}/${config.getDatabaseName()}?retryWrites=true&w=1`;
      this.logger.log(`Host: ${config.getDatabaseHost()}`);
      this.logger.log(`Name: ${config.getDatabaseName()}`);
    }
  }

  createMongooseOptions():
    | import('@nestjs/mongoose').MongooseModuleOptions
    | Promise<import('@nestjs/mongoose').MongooseModuleOptions> {
    return {
      uri: this.uri,
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex: true,
    };
  }
}
