import { Test, TestingModule } from '@nestjs/testing';
import { EnvironmentVariablesService } from './environmentVariables.service';

describe('Environment Variables Service', () => {
  let environmentVariablesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: EnvironmentVariablesService,
          useValue: new EnvironmentVariablesService(
            `${process.env.NODE_ENV}.env`,
          ),
        },
      ],
    }).compile();

    environmentVariablesService = module.get<EnvironmentVariablesService>(
      EnvironmentVariablesService,
    );
  });

  it('Should be defined', () => {
    expect(environmentVariablesService).toBeDefined();
  });

  it('Environment not exist', async () => {
    try {
      await environmentVariablesService.validateInput();
    } catch (error) {
      expect(error.message).toEqual(
        'Config validation error: child "DATABASE_HOST" fails because ["DATABASE_HOST" is required]',
      );
    }
  });

  it('Get specific key', async () => {
    const DATABASE_HOST = await environmentVariablesService.get(
      'DATABASE_HOST',
    );
    expect(DATABASE_HOST).toEqual(expect.anything());
  });

  it('Get NodeEnv', async () => {
    const NodeEnv = await environmentVariablesService.getNodeEnv();
    expect(NodeEnv).toEqual(expect.anything());
  });

  it('Get Port', async () => {
    const Port = await environmentVariablesService.getPort();
    expect(Port).toEqual(expect.anything());
  });

  it('Get DatabaseHost', async () => {
    const DatabaseHost = await environmentVariablesService.getDatabaseHost();
    expect(DatabaseHost).toEqual(expect.anything());
  });

  it('Get DatabaseName', async () => {
    const DatabaseName = await environmentVariablesService.getDatabaseName();
    expect(DatabaseName).toEqual(expect.anything());
  });
});
