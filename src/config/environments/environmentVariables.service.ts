import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from '@hapi/joi';

export interface EnvConfig {
  [key: string]: string;
}

export class EnvironmentVariablesService {
  private readonly envConfig: EnvConfig;

  /**
   * Load a file with configuration variables.
   * @param filePath The file path
   */
  constructor(filePath: string) {
    let config;

    if (fs.existsSync(filePath)) {
      config = dotenv.parse(fs.readFileSync(filePath));
    }

    this.envConfig = this.validateInput(config);
  }

  /**
   * validate the variables declared within the environment variables file.
   * @param envConfig
   * @return object with configuration variables
   */
  private validateInput(envConfig: EnvConfig = {}): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'staging'])
        .default('development'),
      PORT: Joi.number().default(3000),
      DATABASE_HOST: Joi.string().required(),
      DATABASE_PORT: Joi.number(),
      DATABASE_USERNAME: Joi.string(),
      DATABASE_PASSWORD: Joi.string(),
      DATABASE_NAME: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );

    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }

    return validatedEnvConfig;
  }

  /**
   * Get value stored in the environment variables file.
   * @param key The variable key
   * @return The current value
   */
  get(key: string): string {
    return this.envConfig[key];
  }

  /**
   * Get NODE_ENV value stored in the environment variables file.
   * @return The current value for NODE_ENV
   */
  getNodeEnv(): string {
    return this.envConfig.NODE_ENV;
  }

  /**
   * Get PORT value stored in the environment variables file.
   * @return The current value for PORT
   */
  getPort(): number {
    return Number(this.envConfig.PORT);
  }

  /**
   * Get DATABASE_HOST value stored in the environment variables file.
   * @return The current value for DATABASE_HOST
   */
  getDatabaseHost(): string {
    return this.envConfig.DATABASE_HOST;
  }

  /**
   * Get DATABASE_PORT value stored in the environment variables file.
   * @return The current value for DATABASE_PORT
   */
  getDatabasePort(): number {
    return Number(this.envConfig.DATABASE_PORT);
  }

  /**
   * Get DATABASE_USERNAME value stored in the environment variables file.
   * @return The current value for DATABASE_USERNAME
   */
  getDatabaseUsername(): string {
    return this.envConfig.DATABASE_USERNAME;
  }

  /**
   * Get DATABASE_PASSWORD value stored in the environment variables file.
   * @return The current value for DATABASE_PASSWORD
   */
  getDatabasePassword(): string {
    return this.envConfig.DATABASE_PASSWORD;
  }

  /**
   * Get DATABASE_NAME value stored in the environment variables file.
   * @return The current value for DATABASE_NAME
   */
  getDatabaseName(): string {
    return this.envConfig.DATABASE_NAME;
  }
}
