import { Module, Global, Logger } from '@nestjs/common';
import { EnvironmentVariablesService } from './environmentVariables.service';

Logger.log(process.env.NODE_ENV);

@Global()
@Module({
  providers: [
    {
      provide: EnvironmentVariablesService,
      useValue: new EnvironmentVariablesService(
        `${process.env.NODE_ENV || 'development'}.env`,
      ),
    },
  ],
  exports: [EnvironmentVariablesService],
})
export class EnvironmentVariablesModule {}
