// Validators DTO
export const MIN_PASSWORD = 6;
export const MAX_PASSWORD = 24;

// Regex
export const REGEX_EMAIL = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
export const REGEX_PASSWORD = /^((?=.*\d)(?=.*[A-Z])(?=.*\W).{6,})$/;

// Security
export const SALT_ROUNDS_HASH_PASSWORD = 10;
