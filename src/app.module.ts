import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './api/user.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseConfigService } from './config/database/mongoose.config.service';
import { EnvironmentVariablesModule } from './config/environments/environmentVariables.module';

@Module({
  imports: [
    UserModule,
    EnvironmentVariablesModule,
    MongooseModule.forRootAsync({
      useClass: MongooseConfigService,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
