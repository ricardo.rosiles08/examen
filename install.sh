#!/bin/bash

RESET="\033[0m"
RED="\033[1;31m"
MAGENTA="\033[1;35m"

DELIMITTER="========================================="

function log_info() {
  echo -e "$BLUE[$(date --iso-8601=seconds)] $1$RESET"
}

function log_split() {
  echo -e "$MAGENTA$DELIMITTER\n[$(date --iso-8601=seconds)]\n$DELIMITTER$RESET"
}

log_split
log_info "Remove dist"
if [ -d "dist" ]; then
  rm -rf dist
fi
