<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>
  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
<p align="center">
  <img src="https://img.shields.io/badge/npm-6.9.0-red" alt="NPM Version" />
  <img src="https://img.shields.io/badge/node.js-12.14.1-green" alt="NodeJS Version" />
</p>

2. Create a environment file (Environments section).
```bash
$ bash install.sh
```

## Running the app
```bash
# development
$ npm run start
# watch mode
$ npm run start:dev

```
## Test
```bash
# unit tests
$ npm run test
# watch mode
```


## Environments
Create a file with **.env** extension depending on the execution mode.
- development.env
- test.env
